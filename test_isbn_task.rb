class ISBNCalculator
  def initialize(isbn)
    @isbn = isbn
  end

  def calculate
    unless valid_isbn_format?
      puts "Invalid ISBN-13 format. Please provide a valid 13-digit ISBN."
      return nil
    end

    digits = @isbn.chars.map(&:to_i)
    sum = 0

    digits.each_with_index do |digit, index|
      multiplier = (index % 2 == 0) ? 1 : 3
      sum += digit * multiplier
    end

    check_digit = (10 - (sum % 10)) % 10
    @isbn + check_digit.to_s
  end

  private

  def valid_isbn_format?
    return false unless @isbn.match(/^\d{12}$/)
    return false unless @isbn.chars.all? { |char| char.match?(/\d/) }
    true
  end
end


original_isbn = "978014300723"
calculator = ISBNCalculator.new(original_isbn)
calculated_isbn = calculator.calculate

if calculated_isbn
  puts "Full ISBN number : #{calculated_isbn}"
else
  puts "Calculation failed."
end
