require_relative '../test_isbn_task'

describe ISBNCalculator do
  describe '#calculate' do
    it 'returns a valid ISBN-13' do
      calculator = ISBNCalculator.new("978014300723")
      result = calculator.calculate
      expect(result).to eq("9780143007234")
    end

    it 'returns nil for an invalid ISBN format' do
      calculator = ISBNCalculator.new("978014300723й")
      result = calculator.calculate
      expect(result).to be_nil
    end
  end
end